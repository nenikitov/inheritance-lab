public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[] {
            new ElectronicBook("Getting gud", "From software", 102400),
            new Book("Breathing 101", "John Breatherson"),
            new ElectronicBook("How to get Faster Pizza", "Pizza Hut Official", 2),
            new Book("Memes", "Reddit"),
            new ElectronicBook("Yes", "The Acceptance Team", 3),
        };
    }
}
